<?php
/**
 * Created by PhpStorm.
 * User: neelay.u
 * Date: 05-Dec-15
 * Time: 12:40 PM
 */

require_once 'resizer.php';
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-2">
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/insertResident.php">Insert Resident</a>
            </div>
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/paymentAmount.php">Update Payment amount</a>
            </div>
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/numberOfResident.php">View Number of Residents</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-2">
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/average.php">Average monthly spend</a>
            </div>
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/paymentType.php">Payment Type</a>
            </div>
            <div class="col-xs-3">
                <a class="btn btn-primary btn-block" href="pages/lease.php">Lease ending</a>
            </div>
        </div>
    </div>
</div>
