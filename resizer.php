<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require 'vendor/autoload.php';
$source = 'mehndi.jpg';
$image = ImageCreateFromJPEG($source);
$destination = 'resize.jpg';
$width = ImageSx($image);
$height = ImageSy($image);
$headers = apache_request_headers();
$w=158;
$h=148;
if(($w - $width) > ($h - $height)) { // use height
	$s = $h / $height;
	$nw = round($width * $s);
	$nh = round($height * $s);
}
else { // Use width
	$s = $w / $width;
	$nw = round($width * $s);
	$nh = round($height * $s);
}


$imagine   = new Imagine\Gd\Imagine();
$size      = new Imagine\Image\Box($nw, $nh);
$mode      = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
$resizeimg = $imagine->open($source)
                ->thumbnail($size, $mode);
$sizeR     = $resizeimg->getSize();
$widthR    = $sizeR->getWidth();
$heightR   = $sizeR->getHeight();


$preserve  = $imagine->create($size);
$startX = $startY = 0;
$options = array(
    'resolution-units' => Imagine\Image\ImageInterface::RESOLUTION_PIXELSPERINCH,
    'resolution-x' => 150,
    'resolution-y' => 120,
    'resampling-filter' => Imagine\Image\ImageInterface::FILTER_LANCZOS,
);
$preserve->paste($resizeimg, new Imagine\Image\Point($startX, $startY))
    ->save($destination,$options);
?>
